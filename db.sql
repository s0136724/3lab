CREATE TABLE application (
id int(10) unsigned NOT NULL AUTO_INCREMENT,
name varchar(128) NOT NULL DEFAULT '',
email varchar(256) NOT NULL,
age int(4) UNSIGNED NOT NULL,
sex varchar(6) NOT NULL,
limbs int(2) UNSIGNED NOT NULL,
powers varchar(512) NULL,
bio varchar(512) NOT NULL,
PRIMARY KEY (id)
);